import React, { Component } from 'react';
import {Form, Input, Row, Col, Button} from 'muicss/react';

class AddTask extends Component {
  constructor(props){
    super(props);
    this.state = {
      task: ''
    }
  }

  onSubmit(ev){
    this.props.onAddTask(this.state.task);
    this.state.task = '';
    ev.preventDefault();
  }

  onChange(ev){
    this.setState({task:ev.target.value});
  }

  render() {
    let btncyan = {color: 'white', background: '#00BCD4'}

    return (
      <Form onSubmit={this.onSubmit.bind(this)}>
        <Row>
          <Col md="8">
            <Input label="Add Task" value={this.state.task} floatingLabel={true}
                   onChange={this.onChange.bind(this)} />
          </Col>
          <Col md="2">
            <Button variant="raised" style={btncyan}>Submit</Button>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default AddTask;
