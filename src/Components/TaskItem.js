import React, { Component } from 'react';
import {Checkbox, Row, Col, Button} from 'muicss/react';

class TaskItem extends Component {
  constructor(props){
    super(props);
    this.state = {
      task: props.task
    }
  }

  onChange(task, ev){
      this.props.onEditState(task, ev.target.checked);
  }

  handleClearTask(task){
      this.props.onClearTask(task);
      console.log(task)
  }

  render() {
    let right = {textAlign: 'right'}
    let btn = {color: '#DD2C00', background: '#E0E0E0'}
    let inpad = {padding: '5px'}

    let clearBtn;
    if (this.state.task.completed){
      clearBtn =
        <Col md="2">
          <div style={inpad}>
            <Button variant="raised" style={btn} onClick={this.handleClearTask.bind(this, this.state.task)}>
              Dispose Task
            </Button>
          </div>
        </Col>
    }

    return (
      <Row>
        <Col md="10">
          <div className="mui--divider-bottom mui--z2" style={inpad}>
            <Checkbox className={(this.state.task.completed) ? "completed" : ""}
                      onChange={this.onChange.bind(this, this.state.task)} name={this.state.task._id.$oid}
                      label={this.state.task.text} defaultChecked={this.state.task.completed} />
          </div>
        </Col>
        {clearBtn}
      </Row>
    );
  }
}

export default TaskItem;
