import React, { Component } from 'react';
import axios from 'axios';
import {Appbar, Container, Button} from 'muicss/react';
import Tasks from './Components/Tasks';
import AddTask from './Components/AddTask';
import './App.css';

class App extends Component {
    constructor(){
        super();
        this.state = {
            tasks:[]
        }
    }

    componentWillMount(){
        this.getTasks();
    }

    getTasks(){
        axios.request({
            method:'get',
            url:'https://api.mlab.com/api/1/databases/reacttasks/collections/tasks?apiKey=7cYrOVWC5bLY_zl08CW_nXpMYNxqHd9R'
        }).then((response) => {
            this.setState({tasks: response.data}, () => {
                console.log(this.state);
            });
        }).catch((error) => {
            console.log(error);
        });
    }

    editState(task, checked){
        console.log(task)
        axios.request({
            method:'put',
            url:'https://api.mlab.com/api/1/databases/reacttasks/collections/tasks/'+task._id.$oid
                +'?apiKey=7cYrOVWC5bLY_zl08CW_nXpMYNxqHd9R',
            data: {
                $set: {completed: checked}
            }
        }).then((response) => {
            console.log(response)
            let tasks = this.state.tasks;
            for(let i = 0; i < tasks.length; i++){
                if(tasks[i]._id.$oid === response.data._id.$oid){
                    tasks[i].completed = checked;
                }
            }
            this.setState({tasks: tasks});
        }).catch((error) => {
            console.log(error);
        });
    }

    addTask(text){
        if(text.length < 3){
            return
        }
        axios.request({
            method:'post',
            url:'https://api.mlab.com/api/1/databases/reacttasks/collections/tasks?apiKey=7cYrOVWC5bLY_zl08CW_nXpMYNxqHd9R',
            data: {
                text: text,
                completed: false
            }
        }).then((response) => {
            let tasks = this.state.tasks;
            tasks.push({
                _id: response.data._id,
                text: text,
                completed: false
            });
            this.setState({tasks: tasks});
        }).catch((error) => {
            console.log(error);
        });
    }

    clearTask(task){
        let tasks = this.state.tasks;
        for(let i = 0; i < tasks.length; i++){
            if(tasks[i]._id.$oid === task._id.$oid){
                tasks.splice(i, 1);
                axios.request({
                    method:'delete',
                    url:'https://api.mlab.com/api/1/databases/reacttasks/collections/tasks/'+task._id.$oid
                        +'?apiKey=7cYrOVWC5bLY_zl08CW_nXpMYNxqHd9R'
                }).then((response) => {

                }).catch((error) => {
                    console.log(error);
                });
            }
        }
        this.setState({tasks: tasks});
    }

    clearAllTasks(){
        let tasks = this.state.tasks;
        let i = tasks.length;

        while(i--){
            if(tasks[i].completed === true){
                let id = tasks[i]._id.$oid;
                tasks.splice(i, 1);
                // axios.request({
                //     method:'delete',
                //     url:'https://api.mlab.com/api/1/databases/reacttasks/collections/tasks/'+id+'?apiKey=7cYrOVWC5bLY_zl08CW_nXpMYNxqHd9R'
                // }).then((response) => {
                //
                // }).catch((error) => {
                //     console.log(error);
                // });
            }
        }
        this.setState({tasks: tasks});
    }

    render() {
        let s1 = {verticalAlign: 'middle'};
        let s2 = {textAlign: 'center'};
        let btnred = {color: 'white', background: '#DD2C00'}

        return (
            <div className="App">
                <Appbar>
                    <Container>
                        <table width="100%">
                            <tbody>
                            <tr style={s1}>
                                <td className="mui--appbar-height" style={s2}><h3>React Tasks</h3></td>
                            </tr>
                            </tbody>
                        </table>
                    </Container>
                </Appbar>
                <br />
                <Container>
                    <AddTask onAddTask={this.addTask.bind(this)} />
                    <Tasks onEditState={this.editState.bind(this)}
                           onClearTask={this.clearTask.bind(this)}
                           tasks={this.state.tasks} />
                    <Button style={btnred} variant="raised" onClick={this.clearAllTasks.bind(this)}>
                        Clear All Tasks
                    </Button>
                </Container>
            </div>
        );
    }
}

export default App;
